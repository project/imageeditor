<?php
/**
 * @file
 * Ribbet editor for Image Editor module.
 */

/**
 * Plugin declaration.
 */
$plugin = array(
  'name' => t('Ribbet'),
  'description' => t('Advanced editor'),
  'class' => 'ribbet',
  'site' => 'http://www.ribbet.com/',
  'api_key' => FALSE,
  'image_creation' => TRUE,
  'launch_type' => 'overlay',
  'image_url_param' => '_import',
  'options' => array(
    '_export' => url(imageeditor_ajax_save_path() . '/ribbet', array('absolute' => TRUE)),
    '_exclude' => 'out,home',
    '_export_title' => 'Save to ' . variable_get('site_name', ''),
    '_export_agent' => 'browser',
    'embed' => 'true',
  ),
  'initialize_callback' => 'imageeditor_ribbet_initialize_callback',
  'save_callback' => 'imageeditor_ribbet_save_callback',
  'js' => 'ribbet.js',
  'css' => 'ribbet.css',
);

function imageeditor_ribbet_initialize_callback(&$editor) {
  global $is_https;
  $editor['options']['launch_url'] = ($is_https ? 'https' : 'http') . '://www.ribbet.com/app/';
}

function imageeditor_ribbet_save_callback() {
  $image = '';
  if (isset($_GET['file'])) {
    $directory = imageeditor_temporary_directory();
    $data = drupal_http_request(rawurldecode($_GET['file']));
    $destination = $directory . '/' . $_GET['title'] . '.' . $_GET['type'];
    $file = file_unmanaged_save_data($data->data, $destination, FILE_EXISTS_REPLACE);
    $image = file_create_url($file);
  }
  $js_code = 'if (parent) {';
  $js_code .= 'parent.Drupal.settings.imageeditor.save.image = "' . $image . '";';
  $js_code .= 'parent.Drupal.imageeditor.save();';
  $js_code .= 'parent.Drupal.imageeditor.overlay.hide();';
  $js_code .= '}';
  drupal_add_js($js_code, 'inline');
}
